package videocon.connect;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by inderbagga on 12/09/17.
 */
public class HotListAdapter extends BaseAdapter {

    ArrayList<HotItem> mHotspots;
    LayoutInflater mInflater;

    public HotListAdapter(Context context, ArrayList<HotItem> alHotspots) {

        mHotspots=alHotspots;
        mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mHotspots.size();
    }

    @Override
    public Object getItem(int i) {
        return mHotspots.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {

        View vi = convertView;

        if (convertView == null) vi = mInflater.inflate(R.layout.item_list_hotspots, null);

        final TextView tvLocationName = (TextView) vi.findViewById(R.id.locationName);
        final TextView tvLocationDescription = (TextView) vi.findViewById(R.id.locationDescription);

        tvLocationName.setText(mHotspots.get(i).getLocationName());
        tvLocationDescription.setText(mHotspots.get(i).getLocationDescription());

        return vi;
    }
}
