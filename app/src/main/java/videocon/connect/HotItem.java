package videocon.connect;

/**
 * Created by inderbagga on 12/09/17.
 */

public class HotItem {

    private String locationDescription;
    private String locationName;
    private String offlineMessage;
    private String category;
    private String fieldName;
    private int locationId;
    private double longitude;
    private double latitude;

    public HotItem(String locationDescription, String locationName, String offlineMessage, String category, String fieldName, int locationId, double latitude, double longitude) {
        this.locationDescription=locationDescription;
        this.locationName=locationName;
        this.offlineMessage=offlineMessage;
        this.category=category;
        this.fieldName=fieldName;
        this.locationId=locationId;
        this.longitude=longitude;
        this.latitude=latitude;
    }

    public String getLocationDescription() {
        return locationDescription;
    }

    public void setLocationDescription(String locationDescription) {
        this.locationDescription = locationDescription;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getOfflineMessage() {
        return offlineMessage;
    }

    public void setOfflineMessage(String offlineMessage) {
        this.offlineMessage = offlineMessage;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public int getLocationId() {
        return locationId;
    }

    public void setLocationId(int locationId) {
        this.locationId = locationId;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }
}
