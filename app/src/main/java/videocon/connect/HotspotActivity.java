package videocon.connect;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.elitecore.wifi.listener.OnWiFiTaskCompleteListner;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by inderbagga on 08/09/17.
 */

public class HotspotActivity extends AppCompatActivity implements OnWiFiTaskCompleteListner {

    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    ProgressDialog mProgressDialog;
    ArrayList<HotItem> alHotspots=new ArrayList<>();
    ArrayList<Marker> alMarkers=new ArrayList<>();
    ListView oListView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotspots);

        mProgressDialog=new ProgressDialog(HotspotActivity.this);
        mProgressDialog.setMessage("Getting Hotspots ...");
        mProgressDialog.setIndeterminate(true);

        oListView=(ListView)findViewById(R.id.listView);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                .permitAll().build();
        StrictMode.setThreadPolicy(policy);

        OkHttpClient okHttpClient=new OkHttpClient();

        try{

            JSONObject requestObject=new JSONObject();
            requestObject.put("operatingSystem","ANDROID");
            requestObject.put(Backend.SECRET_KEY,getSharedPreferences("Cache",MODE_PRIVATE).getString(Backend.SECRET_KEY,""));
            requestObject.put(Backend.UID_KEY,getSharedPreferences("Cache",MODE_PRIVATE).getString(Backend.UID_KEY,""));

            Log.d("requestObject",requestObject.toString());

            RequestBody body = RequestBody.create(JSON, requestObject.toString());
            Request request = new Request.Builder()
                    .url(Backend.getSyncLocationApi())
                    .post(body)
                    .build();

            mProgressDialog.show();
            okHttpClient.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    e.printStackTrace();
                    mProgressDialog.dismiss();
                }

                @Override
                public void onResponse(Call call, final Response response) throws IOException {

                    mProgressDialog.dismiss();

                    if (!response.isSuccessful()) {
                        throw new IOException("Unexpected code " + response);
                    } else {

                        HotspotActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                try{

                                    final String strResponse=response.body().string();

            // Get the SupportMapFragment and request notification
            // when the map is ready to be used.
            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map);


            mapFragment.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(final GoogleMap googleMap) {

                    try{

                        alHotspots.clear();

                        //googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                        googleMap.getUiSettings().setZoomControlsEnabled(true);
                        googleMap.getUiSettings().setCompassEnabled(true);
                        googleMap.getUiSettings().setMyLocationButtonEnabled(true);
                        googleMap.getUiSettings().setMapToolbarEnabled(true);
                        googleMap.getUiSettings().setZoomGesturesEnabled(true);
                        googleMap.getUiSettings().setScrollGesturesEnabled(true);
                        googleMap.getUiSettings().setTiltGesturesEnabled(true);
                        googleMap.getUiSettings().setRotateGesturesEnabled(true);

                                              JSONObject responseObject=new JSONObject(strResponse);
                                                JSONArray responseData=responseObject.getJSONArray("responseData");

                                                for(int index=0;index<responseData.length();index++){

                                                     String locationDescription=responseData.getJSONObject(index).getString("locationDescription");
                                                     String locationName=responseData.getJSONObject(index).getString("locationName");
                                                     String offlineMessage=responseData.getJSONObject(index).getString("offlineMessage");
                                                     String category=responseData.getJSONObject(index).getString("category");
                                                     String fieldName=responseData.getJSONObject(index).getString("fieldName");
                                                     int locationId=responseData.getJSONObject(index).getInt("locationId");;
                                                     double longitude=responseData.getJSONObject(index).getDouble("longitude");
                                                     double latitude=responseData.getJSONObject(index).getDouble("latitude");

                                                    LatLng mLocationPoint=new LatLng(latitude,longitude);
                                                    googleMap.addMarker(new MarkerOptions().position(mLocationPoint).title(locationName));

                                                    HotItem hotItem=new HotItem(locationDescription,locationName,offlineMessage,category,fieldName,locationId,latitude,longitude);
                                                    alHotspots.add(hotItem);


                                                    Marker pos= googleMap.addMarker(new MarkerOptions().position(new LatLng(alHotspots.get(index).getLatitude(),alHotspots.get(index).getLongitude())).title(alHotspots.get(index).getLocationName()));
                                                    alMarkers.add(pos);

                                                }

                     /*   com.videocon.sterlite.HotItem hotItem1=new com.videocon.sterlite.HotItem("Mohali Tower is Good Commercial Location.","Mohali Tower","Welcome to Mohali Tower.","Commercial Building","Mohali Tower",21,30.70868898,76.69520259);
                        com.videocon.sterlite.HotItem hotItem2=new com.videocon.sterlite.HotItem("THis is demo Site to test the Near by WiFi.","Godrej Plant","Welcome to Mohali Tower.","Commercial Building","Mohali Tower",21,30.70332024,76.69877529);

                        com.videocon.sterlite.HotItem hotItem3=new com.videocon.sterlite.HotItem("My Office Location","My Location","Welcome to Mohali Tower.","Commercial Building","Mohali Tower",21,30.71311658,76.69483781);
                        com.videocon.sterlite.HotItem hotItem4=new HotItem("Blue Dart  Service.","Blue Dart","Welcome to Mohali Tower.","Commercial Building","Mohali Tower",21,30.7076374,76.68966651);

                        Marker pos1= googleMap.addMarker(new MarkerOptions().position(new LatLng(hotItem1.getLatitude(),hotItem1.getLongitude())).title(hotItem1.getLocationName()));
                        Marker pos2=googleMap.addMarker(new MarkerOptions().position(new LatLng(hotItem2.getLatitude(),hotItem2.getLongitude())).title(hotItem2.getLocationName()));
                        Marker pos3=googleMap.addMarker(new MarkerOptions().position(new LatLng(hotItem3.getLatitude(),hotItem3.getLongitude())).title(hotItem3.getLocationName()));
                        Marker pos4=googleMap.addMarker(new MarkerOptions().position(new LatLng(hotItem4.getLatitude(),hotItem4.getLongitude())).title(hotItem4.getLocationName()));

                        alHotspots.add(hotItem1);
                        alHotspots.add(hotItem2);
                        alHotspots.add(hotItem3);
                        alHotspots.add(hotItem4);

                        alMarkers.add(pos1);
                        alMarkers.add(pos2);
                        alMarkers.add(pos3);
                        alMarkers.add(pos4);*/

                        final LatLng _LocationPoint=new LatLng(alHotspots.get(0).getLatitude(),alHotspots.get(0).getLongitude());

                        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(_LocationPoint, 14.0f));
                        googleMap.setBuildingsEnabled(true);

                        oListView.setAdapter(new HotListAdapter(HotspotActivity.this,alHotspots));

                        oListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                                LatLng _LocationPoint=new LatLng(alHotspots.get(i).getLatitude(),alHotspots.get(i).getLongitude());

                                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(_LocationPoint, 14.0f));

                                alMarkers.get(i).showInfoWindow();

                            }
                        });

                    }catch(Exception je){
                        displayMessage(je.getMessage(),-1);
                    }
                }
            });

        }catch (Exception e){
            displayMessage(e.getMessage(),-1);
        }
                            }
                        });
                    }
                }
            });
        }
        catch(JSONException je){displayMessage(je.getMessage(),-1);}
        catch(Exception e){displayMessage(e.getMessage(),-1);}
    }

    private void displayMessage(String message,final int code){

        AlertDialog.Builder dialog=new AlertDialog.Builder(HotspotActivity.this);
        dialog.setCancelable(false);
        dialog.setMessage(message);

        dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();

            }
        });
        dialog.show();

    }

    @Override
    public void onWiFiTaskComplete(String s) {
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void isWiFiInRange(boolean b) {

    }

    @Override
    public void getResponseData(String s) {
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onWiFiScanComplete(List<String> list) {

    }
}
