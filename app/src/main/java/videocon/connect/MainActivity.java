package videocon.connect;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.elitecore.elitesmp.listener.OnEliteSMPTaskCompleteListner;
import com.elitecore.elitesmp.pojo.Plan;
import com.elitecore.elitesmp.utility.ElitePropertiesUtil;
import com.elitecore.wifi.api.EliteWiFiAPI;
import com.elitecore.wifi.api.IWiFiConfiguration;
import com.elitecore.wifi.listener.OnWiFiTaskCompleteListner;
import com.elitecorelib.core.EliteSession;
import com.elitecorelib.core.pojo.PojoSubscriber;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements OnEliteSMPTaskCompleteListner,OnWiFiTaskCompleteListner {

    ProgressDialog mProgressDialog;
    SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mProgressDialog=new ProgressDialog(MainActivity.this);
        mProgressDialog.setMessage("Registering ...");
        mProgressDialog.setIndeterminate(true);

        preferences=getSharedPreferences("Cache",MODE_PRIVATE);

        EliteSession.setELiteConnectSession(this);
        try{
            ElitePropertiesUtil.createInstance(R.raw.elitesmp);
        }catch (Exception e){
            e.printStackTrace();
        }

    }


    public void doRegister(View v){

        mProgressDialog.show();

        IWiFiConfiguration api=new EliteWiFiAPI(MainActivity.this);

        try{

            PojoSubscriber mPojoSubscriber=new PojoSubscriber();
            mPojoSubscriber.setUserName("xyz");
            mPojoSubscriber.setFirstName("in");
            mPojoSubscriber.setLastName("be");
            mPojoSubscriber.setGender("male");

            api.doRegistration(Backend.getSharedKey(),mPojoSubscriber);

        }catch(Exception e){
            EliteSession.eLog.c("TAG"," Error while Monetization Registration"+e.getMessage());
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void getPackageList(List<Plan> list, int i) {

    }

    @Override
    public void getResponseMap(Map<String, String> map, int i) {

    }

    @Override
    public void getGenericResponse(String s, int i) {

    }

    @Override
    public void onWiFiTaskComplete(String s) {

    }

    @Override
    public void isWiFiInRange(boolean b) {

    }

    @Override
    public void getResponseData(String s) {

        mProgressDialog.dismiss();
        Log.e("sample_response",s);

        try{

            JSONObject jsonObject=new JSONObject(s);

            int responseCode=jsonObject.getInt("responseCode");
            String responseMessage=jsonObject.getString("responseMessage");

            switch (responseCode){

                case 1:

                    preferences.edit().putString(Backend.SECRET_KEY,jsonObject.getString(Backend.SECRET_KEY)).commit();
                    preferences.edit().putString(Backend.UID_KEY,jsonObject.getString(Backend.UID_KEY)).commit();
                    displayMessage(responseMessage,responseCode);
                    break;
                default:
                    displayMessage(responseMessage,responseCode);
                    break;
            }

        }catch (JSONException je){
            displayMessage(je.getMessage(),-1);
        }catch (Exception e){
            displayMessage(e.getMessage(),-1);
        }
    }

    private void displayMessage(String message,final int code){

        AlertDialog.Builder dialog=new AlertDialog.Builder(MainActivity.this);
        dialog.setCancelable(false);
        dialog.setMessage(message);

        dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();

                if(code==1){
                    startActivity(new Intent(MainActivity.this,HotspotActivity.class));
                }
            }
        });
        dialog.show();

    }

    @Override
    public void onWiFiScanComplete(List<String> list) {

    }
}